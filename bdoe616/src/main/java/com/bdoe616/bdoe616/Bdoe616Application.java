package com.bdoe616.bdoe616;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bdoe616Application {

	public static void main(String[] args) {
		SpringApplication.run(Bdoe616Application.class, args);
	}

}
