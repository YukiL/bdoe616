package com.bdoe616.bdoe616.entity;

public class CalendarEntity {

	private final long id;
	private final String content;

	public CalendarEntity(long id, String content) {
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}
}
