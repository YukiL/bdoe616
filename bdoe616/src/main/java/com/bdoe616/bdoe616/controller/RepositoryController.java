package com.bdoe616.bdoe616.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdoe616.bdoe616.service.ListStaticService;

import com.bdoe616.bdoe616.entity.CalendarEntity;

@RestController
@RequestMapping("list")
public class RepositoryController {

	@Autowired
	private ListStaticService calendarStaticService;

	@GetMapping("overview")
	public CalendarEntity calendar(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new CalendarEntity(counter.incrementAndGet(), String.format(template, name));
	}
}
