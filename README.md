# BDOE616

# A venir

Dev le back-end avec la techno Java + Spring Boot

# Informations complémentaires

L'utilisation du **POST** sur chaque route est voué à un aspect de **sécurité**. En effet, l'utilisation d'un GET résulte en l'utilisation de requestParams qui ne respectent pas les normes de sécurités. L'utilisation unique du POST permet également de bloquer toute requête contenent un verbe autre que le POST et permet ainsi de renforcer la sécurité.

Toute route de ce projet contenant des requestParam où un verbe autre que le **POST** fera l'objet d'une réecriture et sera transférer vers un body où un changement de verbe qui ne laissera transparaître aucune donnée en clair dans les URL.

# Listing des routes utilisées par le back

URL_BACK = http://serveur_de_antoine.fr:22222

## S'inscrire(1) / Se connecter(2) / Mot de passe oublié(3) / Se déconnecter(4)

1 URL_BACK/auth/register, POST, RequestBody {username: String, password: String, email: String}

2 URL_BACK/auth/login, POST, RequestBody {email: String, password: String}

3 URL_BACK/auth/forgotPassword, POST, RequestBody {email: String}

4 URL_BACK/auth/logout, POST

## Une fois connecté

Nous prendrons comme acquis que chaque requête devra inclure en **header** le ***userUUID*** ainsi que le **header** ***bearer*** contenant le ***token de connexion de l'utilisateur***.
Si **aucun** RequestParam n'est indiqué dans la description de la route, nul besoin d'en mettre.

Pour rappel un RequestParam s'écrit "?param=VotreParam" à la **fin** de l'url de l'api .

Si dans un RequestBody il est indiqué ***{Data}***, cela signifie que le Back-End s'attend à recevoir **toute** la structure récupérée lors d'un GET avec à l'intérieur de cette structure, les ***informations modifiées*** par l'utilisateur.

## Liste des répertoires

URL_BACK/list/overview, GET, RequestBody: {}

## Choix du répertoire

URL_BACK/list/select, POST, RequestParam: {listUUID: String}, RequestBody : {}

## Editer le répertoire

URL_BACK/list/edit, PUT, RequestBody: {Data}

## Supprimer le répertoire

URL_BACK/list/delete, DELETE, RequestBody: {listUUID: String}

## Créer un répertoire 

URL_BACK/list/create, POST, RequestBody: {name: String}

## Liste des contacts

URL_BACK/list/contact/overview, GET, RequestBody: {listUUID: String}

## Créer un contact

URL_BACK/list/contact/create, POST, RequestBOdy: {listUUID: String, contactName: String, phoneNumber: String, email: String, postal: {city: String, postCode: Number, street: String, complementary: String} }

## Voir détail contact

URL_BACK/list/contact, GET, RequestParam: {contactUUID: String}

## Supprimer un contact

URL_BACK/list/contact/delete, DELETE, RequestBody: {contactUUID: String}

## Editer un contact

URL_BACK/list/contact/edit, PUT, RequestBody: {Data}

## Rechercher un contact

URL_BACK/list/contact/search, POST, RequestParam: {StrSearch: String}
